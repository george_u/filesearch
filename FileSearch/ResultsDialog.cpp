#include "ResultsDialog.h"
#include "resource.h"

#include <CommCtrl.h>
#include <sstream>

HCURSOR ResultsDialog::_waitingCursor;

ResultsDialog::ResultsDialog(HINSTANCE hInst, HWND hWnd, Search* search)
    :
    _parentWnd(hWnd), _hInst(hInst), _searchPtr(search),
_fileCheckedNumber(0), _fileNumber(0)
{}


void ResultsDialog::show() {
    DialogBoxParam(_hInst, MAKEINTRESOURCE(IDD_DIALOG_RESULTS), _parentWnd, _dialogResultsProc, (LPARAM)this);
}

Search* ResultsDialog::searchPtr() const
{
    return (Search*)_searchPtr;
}

INT_PTR CALLBACK ResultsDialog::_dialogResultsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    ResultsDialog* inst;
    switch (message)
    {
    case WM_INITDIALOG:
        _waitingCursor = LoadCursor(NULL, IDC_WAIT);
        // the instance is in lparam
        SetWindowLongPtr(hDlg, GWLP_USERDATA, lParam);
        inst = (ResultsDialog*)lParam;
        // start searching
        inst->_searchPtr->startSearch();
        // onProgress callback
        inst->_searchPtr->setOnProgress([hDlg, inst]() {
            if (inst->_searchPtr->getCheckedFileNumber() > 0) {
                int percent = inst->_searchPtr->getCheckedFileNumber() * 100 / inst->_searchPtr->getFileNumber();
                SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETMARQUEE, FALSE, 0);
                SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETPOS, percent, 0);
            }
            else {
                SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETMARQUEE, TRUE, 0);
            }
            std::wstringstream itemListStrBuf;
            for (auto&& item : inst->_searchPtr->getFoundItems()) // list the found items
            {
                itemListStrBuf << item.fullPath() << L"\r\n";

            }
            SetDlgItemText(
                hDlg,
                IDC_EDIT1,
                itemListStrBuf.str().c_str()
            );
            });

        return (INT_PTR)TRUE;
    case WM_MOUSEMOVE:
        inst = (ResultsDialog*)GetWindowLongPtr(hDlg, GWLP_USERDATA);
        if (!inst->_searchPtr->isFinished())
        {
            SetCursor(_waitingCursor);
        }
        break;
    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            inst = (ResultsDialog*)GetWindowLongPtr(hDlg, GWLP_USERDATA);
            //// finish the thread before closing the dialog

            if (!inst->_searchPtr->isFinished())
                inst->_searchPtr->cancelSearch();

            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
