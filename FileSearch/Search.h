#pragma once

#include "framework.h"
#include <string>
#include <vector>
#include <functional>

#include "FoundItem.h"

class Search
{
public:
	typedef std::function<void()> callbackFunction;
	Search();
	Search(std::wstring str, std::wstring initDirectory, bool fullMatch);
	void startSearch();
	void cancelSearch();
	void setOnProgress(callbackFunction);
	bool isFinished() const;
	std::vector<FoundItem> getFoundItems();
	int getFileNumber() const;
	int getCheckedFileNumber() const;
private:
	DWORD _searchThreadId;
	HANDLE _searchThread;
	bool _cancelThread;
	bool _isFinished;
	std::wstring _searchName;
	std::wstring _initialDir;
	std::vector<FoundItem> _foundItems;
	callbackFunction _onProgress;
	bool _fullMatch;
	int _fileNumber;
	int _fileCheckedNumber;
	static DWORD WINAPI _procSearchFiles(LPVOID lpParam);
};

