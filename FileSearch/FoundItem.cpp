#include "FoundItem.h"

FoundItem::FoundItem(WIN32_FIND_DATA fileData, std::wstring directoryPath)
	: fileData(fileData), directoryPath(directoryPath)
{}

bool FoundItem::isDirectory() const 
{
	return (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
}

std::wstring FoundItem::fullPath() const
{
	return directoryPath + L"\\" + std::wstring(fileData.cFileName);
}

