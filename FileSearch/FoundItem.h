#pragma once

#include "framework.h"
#include <string>

class FoundItem
{
public:
	FoundItem(WIN32_FIND_DATA fileData, std::wstring directoryPath);
	bool isDirectory() const;
	WIN32_FIND_DATA fileData;
	std::wstring directoryPath;
	std::wstring fullPath() const;
};

