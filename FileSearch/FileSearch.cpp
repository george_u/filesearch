// FileSearch.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "FileSearch.h"
#include <CommCtrl.h>

#include <list>
#include <queue>
#include <string>
#include <sstream>

#include "FoundItem.h"
#include "Search.h"
#include "ParameterDialog.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    DialogParamProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    DialogResultsProc(HWND, UINT, WPARAM, LPARAM);
//DWORD WINAPI        ThreadSearchFiles(LPVOID);
//HCURSOR waitingCursor, previousCursor;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FILESEARCH, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FILESEARCH));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FILESEARCH));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_FILESEARCH);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   //ShowWindow(hWnd, nCmdShow);
   ShowWindow(hWnd, FALSE); // *show invisibly
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (message == WM_CREATE)
    {
        // Show the first dialog window
        
        ParameterDialog dlg(hInst, hWnd);
        dlg.show();
        PostQuitMessage(0); // close the invisible window after the dialog is closed
    }
    else if (message == WM_DESTROY)
    {
        PostQuitMessage(0);
    }
    else {
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

//
//TCHAR fnameStrBuf[MAX_PATH];
//DWORD fnameStrSize;
//
//TCHAR locationStrBuf[MAX_PATH];
//DWORD locationStrSize;
//
//Search search;
//// Message handler for the search dialog
//INT_PTR CALLBACK DialogParamProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
//{
//    UNREFERENCED_PARAMETER(lParam);
//    switch (message)
//    {
//    case WM_INITDIALOG:
//        return (INT_PTR)TRUE;
//
//    case WM_COMMAND:
//        if (LOWORD(wParam) == IDOK) {
//            // start the search
//            // set the parameters
//            fnameStrSize = GetDlgItemText(
//                hDlg, // HWND  hDlg,
//                IDC_EDIT1, //int   nIDDlgItem,
//                fnameStrBuf, // LPSTR lpString,
//                MAX_PATH// int   cchMax
//            );
//            locationStrSize = GetDlgItemText(
//                hDlg, // HWND  hDlg,
//                IDC_EDIT_LOCATION, //int   nIDDlgItem,
//                locationStrBuf, // LPSTR lpString,
//                MAX_PATH// int   cchMax
//            );
//            bool fullMatch = SendDlgItemMessage(hDlg, IDC_CHECK1, BM_GETCHECK, 0, 0);
//            search = Search(std::wstring(fnameStrBuf, fnameStrSize), std::wstring(locationStrBuf, locationStrSize), fullMatch);
//           
//            // show the results dialog
//            DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG_RESULTS), hDlg, DialogResultsProc);
//        }
//        else if (LOWORD(wParam) == IDCANCEL)
//        {
//            // close the dialog
//            EndDialog(hDlg, LOWORD(wParam));
//            return (INT_PTR)TRUE;
//        }
//        break;
//    }
//    return (INT_PTR)FALSE;
//}
//
//
//DWORD fileNumber;
//DWORD fileCheckedNumber;
//std::list<FoundItem> fileSet;
//
//INT_PTR CALLBACK DialogResultsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
//{
//    UNREFERENCED_PARAMETER(lParam);
//    switch (message)
//    {
//    case WM_INITDIALOG:
//        // start searching
//        search.startSearch();
//        // onProgress callback
//        search.setOnProgress([hDlg]() {
//			if (search.getCheckedFileNumber() > 0) {
//				int percent = search.getCheckedFileNumber() * 100 / search.getFileNumber();
//				SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETMARQUEE, FALSE, 0);
//				SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETPOS, percent, 0);
//			}
//			else {
//				SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETMARQUEE, TRUE, 0);
//			}
//			std::wstringstream itemListStrBuf;
//			for (auto&& item : search.getFoundItems()) // list the found items
//			{
//				itemListStrBuf << item.fullPath() << L"\r\n";
//
//			}
//			SetDlgItemText(
//				hDlg,
//				IDC_EDIT1,
//				itemListStrBuf.str().c_str()
//			);
//	    });
//        
//        return (INT_PTR)TRUE;
//    case WM_MOUSEMOVE:
//
//        if (!search.isFinished())
//        {
//            previousCursor = SetCursor(waitingCursor);
//        }
//        else {
//            SetCursor(previousCursor);
//        }
//        break;
//    case WM_COMMAND:
//        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
//        {
//            //// finish the thread before closing the dialog
//            
//            if(!search.isFinished())
//                search.cancelSearch();
//            
//            EndDialog(hDlg, LOWORD(wParam));
//            return (INT_PTR)TRUE;
//        }
//        break;
//    }
//    return (INT_PTR)FALSE;
//}