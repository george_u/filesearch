#include "ParameterDialog.h"
#include "resource.h"

#include "ResultsDialog.h"

TCHAR fnameStrBuf[MAX_PATH];
DWORD fnameStrSize;

TCHAR locationStrBuf[MAX_PATH];
DWORD locationStrSize;

ParameterDialog::ParameterDialog(HINSTANCE hInst, HWND hWnd)
    :
    _parentWnd(hWnd), _hInst(hInst) {}

// Message handler for the search dialog
INT_PTR CALLBACK ParameterDialog::_dialogParamProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        // the instance is in lparam
        SetWindowLongPtr(hDlg, GWLP_USERDATA, lParam);
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK) {
            // start the search
            // set the parameters
            fnameStrSize = GetDlgItemText(
                hDlg, // HWND  hDlg,
                IDC_EDIT1, //int   nIDDlgItem,
                fnameStrBuf, // LPSTR lpString,
                MAX_PATH// int   cchMax
            );
            locationStrSize = GetDlgItemText(
                hDlg, // HWND  hDlg,
                IDC_EDIT_LOCATION, //int   nIDDlgItem,
                locationStrBuf, // LPSTR lpString,
                MAX_PATH// int   cchMax
            );
            bool fullMatch = SendDlgItemMessage(hDlg, IDC_CHECK1, BM_GETCHECK, 0, 0);

            ParameterDialog* inst = (ParameterDialog*)GetWindowLongPtr(hDlg, GWLP_USERDATA);
            if (inst != nullptr) {
                inst->_search = Search(std::wstring(fnameStrBuf, fnameStrSize), std::wstring(locationStrBuf, locationStrSize), fullMatch);

                // show the results dialog
                ResultsDialog dlg(inst->_hInst, hDlg, &(inst->_search));
                dlg.show();
            }
            //DialogBox(_hInst, MAKEINTRESOURCE(IDD_DIALOG_RESULTS), hDlg, DialogResultsProc);
        }
        else if (LOWORD(wParam) == IDCANCEL)
        {
            // close the dialog
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void ParameterDialog::show() {
    DialogBoxParam(_hInst, MAKEINTRESOURCE(IDD_DIALOG_PARAM), _parentWnd, _dialogParamProc, (LPARAM)this);
}

Search* ParameterDialog::searchPtr() const
{
    return (Search*)&_search;
}