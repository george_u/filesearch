#pragma once


#include "framework.h"
#include "Search.h"
#include <list>

class ResultsDialog
{
public:
	ResultsDialog(HINSTANCE, HWND, Search*);
	void show();
	Search* searchPtr() const;
private:
	HINSTANCE _hInst;
	HWND _parentWnd;
	Search* _searchPtr;
	static INT_PTR CALLBACK _dialogResultsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

	DWORD _fileNumber;
	DWORD _fileCheckedNumber;
	std::list<FoundItem> _fileSet;
	static HCURSOR _waitingCursor;
};

