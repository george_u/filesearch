#include "Search.h"

#include <queue>
#include <string.h>

Search::Search()
    : _searchName(), _initialDir(), _fullMatch(false), _fileNumber(0), _fileCheckedNumber(0), _foundItems(),
    _cancelThread(false),
    _searchThread(INVALID_HANDLE_VALUE),
    _searchThreadId(-1),
    _isFinished(false),
    _onProgress(nullptr)
{}

Search::Search(std::wstring str, std::wstring initDirectory, bool fullMatch)
	: _searchName(str), _initialDir(initDirectory), _fullMatch(fullMatch), _fileNumber(0), _fileCheckedNumber(0), _foundItems(),
    _cancelThread(false),
    _searchThread(INVALID_HANDLE_VALUE),
    _searchThreadId(-1),
    _isFinished(false),
    _onProgress(nullptr)
{}

void Search::startSearch()
{
    _cancelThread = FALSE;
    _searchThread = CreateThread(
        NULL,                   // default security attributes
        0,                      // use default stack size  
        _procSearchFiles,       // thread function name
        this,          // argument to thread function 
        0,                      // use default creation flags 
        &_searchThreadId);   // returns the thread identifier 

}

void Search::cancelSearch()
{
    _cancelThread = true;
   /* if(_searchThread != INVALID_HANDLE_VALUE)
        WaitForSingleObject(_searchThread, INFINITE);*/ // this caused a deadlock
}


DWORD WINAPI Search::_procSearchFiles(LPVOID lpParam) {
    Search* inst = (Search*)lpParam;
    std::queue<std::wstring> dirQueue1;
    std::queue<std::wstring> dirQueue2;

    dirQueue1.push(inst->_initialDir);

    WIN32_FIND_DATA fileData;
    while (!dirQueue1.empty()) {
        if (inst->_cancelThread) {
            break;
        }
        std::wstring currentFname = dirQueue1.front();
        auto hFind = FindFirstFile((currentFname + L"\\*").c_str(), &fileData);

        dirQueue1.pop();
        if (INVALID_HANDLE_VALUE == hFind) {
            continue;
        }
        // go through the diretory
        do {
            if (inst->_cancelThread) {
                break;
            }
            if ((0 == lstrcmp(fileData.cFileName, L".")) ||
                (0 == lstrcmp(fileData.cFileName, L".."))) {
                continue; // skip this items
            }
            // count an element
            inst->_fileNumber++;
            if (inst->_onProgress)
                inst->_onProgress();

            if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                // this is a category - add it to the queue

                dirQueue1.push(currentFname + L"\\" + std::wstring(fileData.cFileName));
                dirQueue2.push(currentFname + L"\\" + std::wstring(fileData.cFileName));
            }
        } while (FindNextFile(hFind, &fileData));

        FindClose(hFind);
    }
    // compare them with the name given

    dirQueue2.push(inst->_initialDir);
    while (!dirQueue2.empty()) {
        if (inst->_cancelThread) {
            break;
        }
        std::wstring currentFname = dirQueue2.front();
        auto hFind = FindFirstFile((currentFname + L"\\*").c_str(), &fileData);

        dirQueue2.pop();
        if (INVALID_HANDLE_VALUE == hFind) {
            continue;
        }
        // go through the diretory
        do {
            if (inst->_cancelThread) {
                break;
            }
            if ((0 == lstrcmp(fileData.cFileName, L".")) ||
                (0 == lstrcmp(fileData.cFileName, L".."))) {
                continue; // skip this items
            }
            // count an element
            inst->_fileCheckedNumber++;

            // compare the name
            if (inst->_fullMatch)
            {
                if (0 == lstrcmp(fileData.cFileName, inst->_searchName.c_str())) {
                    // the file is found (full name)
                    inst->_foundItems.push_back(FoundItem(fileData, currentFname));

                    if (inst->_onProgress)
                        inst->_onProgress();
                }
            }
            else {
                // find the substring in the filename, returns NULL if fails
                if (nullptr != wcsstr(fileData.cFileName, inst->_searchName.c_str())) {
                    inst->_foundItems.push_back(FoundItem(fileData, currentFname));

                    if (inst->_onProgress)
                        inst->_onProgress();
                }
            }
            
            // TODO partial comparison
        } while (FindNextFile(hFind, &fileData));

        FindClose(hFind);
    }
    inst->_isFinished = true;
    if (inst->_onProgress)
        inst->_onProgress();
    return 0;
}

bool Search::isFinished() const
{
    return _isFinished;
}

void Search::setOnProgress(callbackFunction handler)
{
    _onProgress = handler;
}

// returns a copy of the list of the items found
std::vector<FoundItem> Search::getFoundItems()
{
    return _foundItems;
}

int Search::getFileNumber() const
{
    return _fileNumber;
}

int Search::getCheckedFileNumber() const
{
    return _fileCheckedNumber;
}