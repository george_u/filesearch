#pragma once

#include "framework.h"
#include "Search.h"

class ParameterDialog
{
public:
	ParameterDialog(HINSTANCE, HWND);
	void show();
	Search* searchPtr() const;
private:
	HINSTANCE _hInst;
	HWND _parentWnd;
	Search _search;
	static INT_PTR CALLBACK _dialogParamProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
};

